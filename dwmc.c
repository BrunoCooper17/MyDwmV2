void
setlayoutex(const Arg *arg)
{
	setlayout(&((Arg) { .v = &layouts[arg->i] }));
}

void
viewex(const Arg *arg)
{
	view(&((Arg) { .ui = 1 << arg->ui }));
}

void
viewall(const Arg *arg)
{
	view(&((Arg){.ui = ~0}));
}

void
toggleviewex(const Arg *arg)
{
	toggleview(&((Arg) { .ui = 1 << arg->ui }));
}

void
tagex(const Arg *arg)
{
	tag(&((Arg) { .ui = 1 << arg->ui }));
}

void
toggletagex(const Arg *arg)
{
	toggletag(&((Arg) { .ui = 1 << arg->ui }));
}

void
tagall(const Arg *arg)
{
	tag(&((Arg){.ui = ~0}));
}

void
togglescratchex(const Arg *arg)
{
	togglescratch(&((Arg){.v = scratchpadcmd }));
}

/* signal definitions */
/* signum must be greater than 0 */
/* trigger signals using `xsetroot -name "fsignal:<signame> [<type> <value>]"` */
static Signal signals[] = {
	/* signum           function */
	{ "quit",           quit },

	{ "killclient",     killclient },
	{ "focusstack",     focusstack },
	{ "movestack",      movestack },
	{ "rotatestack",    rotatestack },
	{ "zoom",           zoom },
	{ "focusmaster",    focusmaster },
	{ "incnmaster",     incnmaster },

	{ "setlayout",      setlayout },
	{ "setlayoutex",    setlayoutex },
	{ "setmfact",       setmfact },
//	{ "setcfact",       setcfact },
	{ "focusmon",       focusmon },
	{ "tagmon",         tagmon },

	{ "view",           view },
//	{ "viewall",        viewall },
	{ "viewex",         viewex },
	{ "toggleview",     view },
	{ "toggleviewex",   toggleviewex },
	{ "tag",            tag },
//	{ "tagall",         tagall },
	{ "tagex",          tagex },
	{ "toggletag",      tag },
	{ "toggletagex",    toggletagex },
	{ "togglebar",      togglebar },
	{ "toggleopacity",  toggleopacity },
	{ "togglefloating", togglefloating },
	{ "togglescratch",  togglescratchex },
};
