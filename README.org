* Dynamic Window Manager (dwm)
This repository holds my own build of suckless dynamic window manager

** How to install
Just run the =compile-dwm= script and copy the following files:
+ *dwm.desktop* to */usr/share/xsessions*
+ *startdwm* to */usr/local/bin*
+ *dwmc* to */usr/local/bin/*
Thats it, nothing more.

Checkout my dwmblocks repo for a status bar (this build use it).

